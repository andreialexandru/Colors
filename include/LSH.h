#pragma once

#include <cstdint>


struct LSH
{
    uint8_t l; // luminosity
    uint8_t s; // saturation
    uint16_t h; // hue


    static auto constexpr MAX_L = 128;
    static auto constexpr MAX_S = 64;
    static auto constexpr MAX_H = 768;
    static auto constexpr MAX_COL = LSH::MAX_H / 3;

    LSH() = delete;
};
